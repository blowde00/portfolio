import React from 'react';
import { Routes, Route } from 'react-router-dom';
import App from './App';
import Header from './Components/Header';
import Hello from './Components/Hello';
import About from './Components/About';
import Projects from './Components/Projects';
import Contact from './Components/Contact';
import ProjectPage from './Components/ProjectPage';
import Footer from './Components/Footer';
import HeaderBasic from './Components/Header';


function router() {
  return (
    <Routes>
      <Route path='/' element={<App />}>
        <Route path='' element={
          <>
            <HeaderBasic />
            <Hello />
            <About />
            <Projects />
            <Contact />
            <Footer />
          </>
        }>
        </Route>
        <Route path='/projects/:projectTitle' element={<ProjectPage />}> </Route>
      </Route>
    </Routes>
  )
}

export default router