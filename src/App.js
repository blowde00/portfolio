import Header from './Components/Header';
import Hello from './Components/Hello';
import About from './Components/About';
import Projects from './Components/Projects';
import Contact from './Components/Contact';
import Footer from './Components/Footer';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import { Outlet } from 'react-router-dom';


function App() {
  return (
    <div className="App">
      <Outlet />
    </div>
  );
}

export default App;