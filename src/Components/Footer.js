import React from 'react';
import { Container } from "react-bootstrap";


function Footer() {
  return (
    <Container className='footer-container'>Built and designed by Brandon Lowdermilk</Container>
  )
}

export default Footer