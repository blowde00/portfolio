import React, { useCallback } from 'react';
import Particles from "react-tsparticles";
import { loadFull } from "tsparticles";
import { Container } from "react-bootstrap";
import particlesOptions from "../Data/particles.json";


function Hello() {
  const particlesInit = useCallback(main => {
    loadFull(main);
}, [])

  return (
    <div className="hello-component">
    <Particles className="particles-component" options={particlesOptions} init={particlesInit}/>
    <Container fluid className="hello-container">
      <h1 className="hello-text">Hello there! I'm <span className='emphasis'>Brandon Lowdermilk</span>.</h1>
      <h1 className="hello-text">I'm a full-stack developer.</h1>
    </Container>
    </div>
  )
}

export default Hello