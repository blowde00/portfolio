import React, { useEffect } from 'react';
import { Container, Card } from 'react-bootstrap';
import { Link, useLocation } from 'react-router-dom';

function About() {
  const { pathname, hash, key } = useLocation();

  useEffect(() => {
    if (hash === '') {
      window.scrollTo(0, 0);
    } else {
      setTimeout(() => {
        const id = hash.replace('#', '');
        const element = document.getElementById(id);
        if (element) {
          element.scrollIntoView();
        }
      }, 0);
    }
  }, [pathname, hash, key]);

  return (
    <Container id="about" className="container">
      <h2 className='header'>About</h2>
      <Card className="about-card">
        <Card.Text>
          I have always been passionate about tech and gaming. Recently I decided it was finally time to steer my career towards these passions. I am currently a Software Engineer for State Farm Technology focusing on front-end web projects. Learning to code has also opened my eyes to another passion for UX and finding unique and new techniques to interact with the user. I am always looking for new tricks and skills to add to my arsenal. I am also learning more about accessibility and how to best build interfaces that are fun and suitable for all types of people. It is my passion as well as my eagerness to constantly be learning, and that makes me a great developer.
        </Card.Text>
        <Card.Text>
          Gaming has been a part of my life since I was old enough to hold an NES Light Gun. I moved on to primarily PC gaming in college and built my first PC shortly after that. If I'm not at my PC playing the latest open world sandbox game or an MMO, I'm usually playing D&D with my friends and family. Some of my other hobbies include playing with my dogs, Sandwich and Pizza, and losing to my wife at Magic the Gathering.
        </Card.Text>
      </Card>
    </Container>
  )
}

export default About