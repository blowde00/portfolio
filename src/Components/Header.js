import React from 'react';
import { Container, Nav, Navbar } from "react-bootstrap";
import { FiMenu } from 'react-icons/fi';
import { Link, useNavigate } from 'react-router-dom';

function HeaderBasic() {

  return (
    <Navbar variant='bl' expand="lg">
      <Navbar.Brand href="/"><h1>B</h1></Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav"> <FiMenu id="menu-button" /> </Navbar.Toggle>
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="ms-auto">
          <Nav.Link href="/#about">About</Nav.Link>
          <Nav.Link href="/#projects">Projects</Nav.Link>
          <Nav.Link href="/#contact">Contact</Nav.Link>
          <Nav.Link href="https://bl-portfolio-bucket.s3.us-west-1.amazonaws.com/Brandon_Resume_Latest.pdf" target="_blank" rel="noreferrer">Resume</Nav.Link>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  )
}

export default HeaderBasic