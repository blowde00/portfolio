import React, { useEffect } from 'react';
import { Container, Button } from 'react-bootstrap';
import { Link, useLocation } from 'react-router-dom';



function Contact() {
  const { pathname, hash, key } = useLocation();

  useEffect(() => {
   if (hash === '') {
     window.scrollTo(0, 0);
   } else {
     setTimeout(() => {
       const id = hash.replace('#', '');
       const element = document.getElementById(id);
       if (element) {
         element.scrollIntoView();
       }
     }, 0);
   }
 }, [pathname, hash, key]);

  return (
    <Container id="contact" className="container">
    <h2 className='header'>Contact</h2>
    <a className="btn btn-primary hello-button" href="mailto:lowdermilk1@gmail.com" rel="noreferrer" target="_blank">Say Hello</a> 
    </Container>
  )
}

export default Contact