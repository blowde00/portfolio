import React, { useEffect, useRef } from 'react';
import { Container, Card, Col, Row, ButtonGroup } from 'react-bootstrap';
import projectData from '../Data/projects.json';
import { Link, useLocation } from 'react-router-dom';

function Projects() {
  const projectRef = useRef()
  const { pathname, hash, key } = useLocation();

  useEffect(() => {
    if (hash === '') {
      window.scrollTo(0, 0);
    } else {
      setTimeout(() => {
        const id = hash.replace('#', '');
        const element = document.getElementById(id);
        if (element) {
          element.scrollIntoView();
        }
      }, 0);
    }
  }, [pathname, hash, key]);

  return (
    <Container fluid ref={projectRef} id="projects" className="container">
      <h2 className='header'>Projects</h2>
      <Row className='project-row'>
        {projectData.length > 0 ? projectData.map(project => {
          return (
            <Col className="project-col" key={project.id}>
              <Card className='project-card'>
                <Card.Title>
                  {project.title}
                </Card.Title>
                <Card.Text className="project-slug"> {project.slug}</Card.Text>
                <ButtonGroup size="lg" aria-label="Project Links" className="button-group">
                  {project.vidLink !== '' ?
                    <Link to={`/projects/${project.title}`} className="btn btn-primary"> Video </Link>
                    : ''}
                  <a href={project.gitLink} target="_blank" rel="noreferrer" className="btn btn-primary"> Code </a>
                </ButtonGroup>
              </Card>
            </Col>
          )
        }) : "Loading"}
      </Row>
    </Container>
  )
}

export default Projects