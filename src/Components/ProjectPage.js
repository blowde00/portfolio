import React from 'react';
import { useParams } from 'react-router-dom';
import projectData from '../Data/projects.json';
import ReactPlayer from 'react-player';
import { MdOutlineKeyboardBackspace } from 'react-icons/md';
import { Container, Accordion } from 'react-bootstrap';
import HeaderBasic from './Header';

function ProjectPage() {
  let params = useParams();
  let project = projectData[projectData.findIndex(matchTitle)];

  function matchTitle(project) {
    return project.title === params.projectTitle
  }

  function navigateBack() {
    window.history.back();
  }


  return (
    <>
      <HeaderBasic />
      <span className="project-return" onClick={navigateBack}> <MdOutlineKeyboardBackspace className='return-icon' /> Back</span>
      <Container fluid className="project-container">
        <ReactPlayer className="project-player"
          url={project.vidLink}
          controls={true}
          width="70vw"
          height=""
        />

        <Accordion defaultActiveKey={project.id} className="project-page-accordion" flush>
          <Accordion.Item eventKey={project.id}>
            <Accordion.Header><h1>{project.title}</h1></Accordion.Header>
            <Accordion.Body>
              <p>
                {project.description}
              </p>
            </Accordion.Body>
          </Accordion.Item>
        </Accordion>
      </Container>
    </>
  )
}

export default ProjectPage